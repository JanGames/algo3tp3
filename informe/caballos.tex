\section{Problema 3: El señor de los caballos}

En un juego de mesa similar al ajedrez, tenemos un tablero de dimension dinámica $n x n$. La idea del juego consiste en ubicar la mínima cantidad de caballos sobre el tablero tal que toda posición del mismo sea alcanzable. Una posición se considera alcanzable si y solo si esta actualmente ocupada por un caballo o es alcanzable por algún caballo en otro casillero. Recordar que en un mismo casillero no puede haber mas de un caballo. Para entender un poco mejor el juego, recordemos los movimientos de un caballo en el ajedrez.

\begin{center}
\chessboard[printarea={a1-e5},
			setwhite={Nc3},
			pgfstyle=cross,
			color = red,
			markfield={b1,d1,a2,e2,a4,e4,b5,d5},
			showmover=false]
\end{center}

Cada caballo tiene como máximo 8 movimientos posibles, que están representados en la imagen por cruces rojas. Es posible que el caballo no pueda ejecutar todos ellos, dado que el movimiento podría caer afuera del tablero.

Al comienzo del juego, se define un conjunto de caballos que ya están ubicados en el tablero. A estos caballos los llamaremos de aquí en adelante caballos estáticos. 

La optimización de nuestro algoritmo deberá tener en cuenta a los caballos estáticos para ubicar la menor cantidad de caballos para cubrir el tablero. Probemos esto con un simple ejemplo, donde los caballos negros representan a los estáticos:

\begin{figure}[ht]
\centering
\begin{minipage}[b]{0.45\linewidth}
\centering
\chessboard[printarea={a1-d4},
			addwhite={Nc2,Nc3,Nd2},
			addblack={Nd3},
			pgfstyle=cross,
			color = red,
			markfield={},
			showmover=false]

\caption{Sin restricción}
\end{minipage}
\quad
\begin{minipage}[b]{0.45\linewidth}
\centering
\chessboard[printarea={a1-d4},
			addwhite={Na1, Nc3, Nc4, Nd3},
			addblack={Nd4},
			pgfstyle=cross,
			color = red,
			markfield={},
			showmover=false]
\caption{Con restricción}
\end{minipage}
\end{figure}

En un tablero de $4x4$, se puede observar que un caballo puede cubrir a lo sumo 4 posiciones (la suya y 3 más). Por lo tanto, el tablero debe tener al menos 4 caballos para cubrirlo por completo. Esto significa que la solución a la figura 1 es óptima.

La solución óptima, dado cualquier conjunto de caballos estáticos, tendrá al menos la misma cantidad de caballos que la solución sin restricciones. En la figura 2 podemos observar como el cubrimiento optimo necesita de 1 caballo más que en la figura 1. Esto se debe a que el caballo estático en el tablero solo puede cubrir 2 casilleros adicionales debido a que esta en una esquina. En la solución sin restricciones, cada caballo cubría 4. Esto demuestra que efectivamente los caballos estáticos son restricciones al momento de buscar el cubrimiento mínimo\footnote{En este caso definimos al cubrimiento mínimo de un tablero como la menor cantidad de caballos necesarios para cubrir todo el tablero.} del tablero, aunque no necesariamente operativas. Esto se debe a que un caballo puede comenzar en una posición optima.

\pagebreak

\subsection{Algoritmo}

A priori, sabemos que el tablero tiene un total de $2^{n^2-k}$ estados posibles. Esto se debe a que el tablero tiene $n^2$ casilleros, y debemos hacer una elección binaria (inserto o no inserto un caballo) sobre $n^2-k$ casilleros.

Para resolver el problema, utilizamos backtracking. Conceptualmente, pensamos al conjunto de todos los tableros posibles a partir de un árbol. El árbol tiene un branching factor de 2, dado que cada casillero puede tener o no un caballo.

Para poder identificar los caballos que nos son dados al iniciar el juego y poder tener una representación útil del tablero, utilizamos la siguiente codificación:
\begin{itemize}
  \item 0: Casillero no ocupado.
  \item 1: Casillero ocupado por un nuevo caballo.
  \item 2: Casillero ocupado por un caballo estático.
\end{itemize}

De esta manera, armamos un árbol de forma recursiva. La codificación es útil para poder mantener el estado actual del árbol a medida que lo recorramos. Comenzamos con el primer casillero, donde hacemos la elección binaria. Si hay un caballo estático en un casillero, lo salteamos, dado que no existe una elección. De esta manera, el árbol tendrá altura $n-k$.

A la derecha de cada nodo interno consideraremos el caso 0, donde no hay un caballo. Luego, a la izquierda consideraremos el caso 1, donde efectivamente ubicamos un caballo. El algoritmo recursivo recorrerá el árbol por DFS arrancando sobre la rama derecha. Esta elección es estratégica, y sera explicada en detalle en la sección de complejidad.

Este algoritmo se puede describir con el siguiente pseudo-código. La función $verifica(tablero)$ verifica si el tablero es una solución. A priori, esta función pertenece a \order{n^2}. Luego de explicar el pseudo-codigo mostraremos porque utilizamos esta función que en principio es sumamente costosa y como DFS mejora la situación de forma dramática. 

El algoritmo toma las siguientes entradas:
\begin{enumerate}
\item $tablero$: Estado actual del tablero utilizando nuestra codificación.
\item $mejorTablero$: Tablero que es una solución local, representado en nuestra codificación.
\item $n$: Tamaño del tablero.
\item $cantCabActual$: Cantidad de caballos que actualmente tiene el tablero.
\item $cantCabMejorS$: Cantidad de caballos que hasta ahora tiene la mejor solución local. Es inicializada en $n^2-k+1$ al comenzar las llamadas recursivas. Le sumamos uno para evitar el caso borde donde el tablero debe ser llenado por completo para ser una solución.
\item $casillaActual$: Numero de casilla actual. Las casillas se recorren de arriba a abajo, de izquierda a derecha.
\end{enumerate}

\begin{algorithmic}
	\Procedure{backtracking}{tablero, mejorTablero, n, cantCabActual, cantCabMejorS, casillaActual}
		\If{$casillaActual < n^2$}
			\State var $i:int\ \leftarrow\ casillaActual\ /\ n$
			\State var $j:int\ \leftarrow\ casillaActual\ \%\ n$
			\If{$tablero[i][j] = 2$}
				\State $backtracking(tablero, mejorTablero, n, cantCabActual, cantCabMejorS, casillaActual + 1)$
			\Else
				\State $backtracking(tablero, mejorTablero, n, cantCabActual, cantCabMejorS, casillaActual + 1)$
				\State $tablero[i][j] \leftarrow 1$
				\If{$verifica(tablero)$}
					\If{$cantCabMejorS > cantCabActual + 1$}
						\State $mejorTablero \leftarrow tablero$
						\State $cantCabMejorS \leftarrow cantCabActual + 1$
					\Else
						\State $backtracking(tablero, mejorTablero, n, cantCabActual, cantCabMejorS, casillaActual + 1)$
					\EndIf
				\State $tablero[i][j] \leftarrow 0$
				\EndIf
			\EndIf
		\EndIf
	\EndProcedure
\end{algorithmic}

Antes de ejecutar esta función, se verifica si el tablero inicial es una solución.

\pagebreak

\subsection{Podas}

Recorrer todos los nodos del árbol claramente no es una buena idea. El árbol tiene $2^{n^2-k}$ nodos, y si recorremos todos nuestro algoritmo sera como mínimo exponencial. Por esta razón debemos elegir podas, eliminando del espacio a recorrer tableros que podemos descartar.

\subsubsection{Óptimo local}
A medida que recorramos el árbol podemos ir guardando las soluciones locales que vamos encontrando. En un principio, consideraremos que el optimo local es $n^2-k$, ya que es el peor de los casos donde llenamos el tablero de caballos. Luego, si sabemos que por una rama no llegaremos a una solución mejor que la local actual, la podemos descartar.

La simetría del tablero hace que esta poda sea sumamente efectiva al momento de recortar el espacio de búsqueda, dado que no debemos recorrer la misma solución hasta 4 veces (hay 4 rotaciones del tablero). A su vez, podemos descartar ramas enteras sin tener que recorrerlas.

\subsubsection{Revisar movimientos antes de agregar un caballo}

Otra posible poda consiste en revisar los casilleros que amenazaría el caballo en caso de ser agregado, si todos ellos ya se encuentran ocupados o amenazados no tiene sentido agregarlo ya que no aportaría a la solución. El beneficio esperado de esta poda es casi mínimo, ya que por cada paso del backtracking seria necesario revisar 8 casilleros. Esto lo veremos experimentalmente mas adelante. La implementación se encuentra en la función $trimTree$ del código.

\subsubsection{Cantidad mínima de caballos necesarios}

Podemos acotar inferiormente a la cantidad de caballos necesarios en un cubrimiento por $n^2/4$. Esto se debe a que en el mejor de los casos, cada caballo cubre 4 posiciones, la suya y tres mas. El hecho de que los caballos se ubiquen en los bordes o que entren en conflicto en cierta posición causa que mas caballos sean necesarios para la solución.

\subsubsection{Cantidad maxima de caballos necesarios}

Por defecto tenemos que inicializar la cota de caballos con el tamaño del tablero($n^2$) ya que es el máximo de caballos que puedo posicionar en el mismo, sin embargo con $n \geq 3$ podemos dar un valor mas pequeño, el cual combinado con la poda mencionada anteriormente tiene el potencial de recortar drasticamente el árbol de decisión. La idea detrás de la cota es encontrar una forma deterministica de posicionar los caballos, de tal manera que si bien puede no ser optima la solucion, la cantidad de caballos sea menor a $n^2$. Si ubicamos los caballos en forma de columna, debido a las posibilidades de movimiento de los mismos, todas las posiciones en las dos columnas anteriores y posteriores a esta se ven amenazadas por lo menos por un caballo. Esto se puede ver en el siguiente caso:

%Si el tamaño del tablero es mayor o igual a 4, podemos dar una cota incial considerablemente mejor que la peor. Para tableros de tamaño 4, 5, 6 y 7 independientemente de los caballos iniciales podemos dar las siguientes configuraciones de caballos:

\begin{center}
\chessboard[style=6x6,
setwhite={Nc1,Nc2,Nc3,Nc4,Nc5,Nc6,Nc7,Nc8,
Nf1,Nf2,Nf3,Nf4,Nf5,Nf6,Nf7,Nf8},
showmover=false,
pgfstyle=cross,
color = red,
markareas={a1-b8,d1-e8,g1-h8}]
\end{center}

Las casillas con una cruz son aquellas a las cuales puede llegar un caballo, como podemos ver, todas las posiciones libres son alcanzables. Esta misma idea se puede extender a un tablero arbitrariamente grande (con $n \geq 3$), colocando cada dos columnas vacías una columna de caballos. Lo podemos generalizar con la siguiente sumatoria:

$$ \sum\limits_{i=1}^{n} \left\lceil\frac{n}{3}\right\rceil = 
n * \left\lceil\frac{n}{3}\right\rceil \leq 
\left\lceil\frac{n^2}{3}\right\rceil $$

\subsection{Complejidad}

La complejidad final esta acotada inferiormente por $\mathcal{O}(2^{n^2 - k})$, siendo $n^2$ la cantidad de casilleros y $k$ la cantidad de caballos estáticos. Esto se debe a que en el peor de los casos tendremos que recorrer y llenar todo el tablero. El caso donde el tablero debe llenarse por completo esta representado por el ultimo nodo que se recorre. Ademas recordar que cada vez que insertamos un caballo, debemos verificar si el nuevo tablero es una solución. A priori, esto pertenece a \order{n^2}. Sin embargo, si llevamos a cabo un análisis amortizado, no siempre pagamos este costo para cada nodo. A continuación presentamos el pseudo-codigo de la funcion verify. La funcion reachable utilizada lo único que hace es codificar el movimiento del caballo, y se ejecuta en \order{1}.

\begin{algorithmic}
	\Procedure{verify}{tablero\& b}
	\State int n = b.size()
	\For{int i = 0; i $<$ n; i++}
			\For{int j = 0; j $<$ n; j++}
        			\If{$\neg$reachable(b, i, j)}
        			\State{return false}
        			\EndIf
       		 \EndFor
    \EndFor
    \State return true;
\EndProcedure
\end{algorithmic}

Al hacer DFS, en primera instancia comenzamos pagando \order{1} en gran cantidad de los primeros nodos. Esto se debe a que el primer nodo del tablero es el primero que se verifica, y uno de los últimos a los que se termina alcanzando. A su vez, al hacer DFS, las instancias de verificación mas costosas, que son las que comienzan en la rama con la mayor cantidad de nodos (ya que todas arrancan siendo soluciones posibles, se arranca de la peor a mejores) se dejan para el final. Esto le da la posibilidad a la poda del optimo a trabajar mejor sobre estas instancias, dado que se puede encontrar un optimo que permita descartarlas. Sin embargo, no podemos evitar que en el peor de los casos hacer backtracking sea exponencial.

\subsection{Variantes del juego}
Existen variantes del juego donde uno debe buscar un cubrimiento mínimo del tablero, aunque usando una cantidad limitada de cada tipo de pieza, no solo caballos. Esta variante introduce algunas particularidades.

\begin{enumerate}
\item Dada una cantidad limitada de piezas, no necesariamente existirá un cubrimiento. Esto se debe a que las piezas pueden no ser suficientes para cubrir todo el tablero.
\item El branching factor de nuestro problema ahora es de 7, dado que podemos usar un peón, una torre, un alfil, un caballo, un rey o una reina para ocupar un casillero. Sin embargo, recordar que tenemos una restricción $r = (c_1, c_2, c_3, c_4, c_5, c_6)$, donde $c_i$ es la cantidad de piezas del tipo $i$ que puedo colocar. Esto significa que el branching factor no necesariamente es constante para toda rama. En el caso que no hay restricciones, sabemos que la reina domina el movimiento de todas las piezas menos el caballo. Por lo tanto en este caso el árbol tendrá $3^{n^2-k}$ nodos. Contar la cantidad de nodos sin considerar la dominancia es un problema difícil. 

A priori, suponiendo que no consideramos la dominancia entre piezas para descartarlas y definiendo a $c$ como la cantidad de piezas totales, el tablero tiene ${{n^2}\choose{min(c,n^2)}}*7^{{n^2}\choose{min(c,n^2)}}$, donde el combinatorio representa la cantidad de grupos en los que puedo agrupar todas las piezas que tengo disponible. Utilizamos la función min por si la cantidad de piezas disponibles es mayor a la cantidad de piezas que entran en el tablero. En pocas palabras, el problema si se lo encara de una forma naïve puede llegar a ser bastante costoso.
\end{enumerate}

\pagebreak

\subsubsection{Podas}
\begin{enumerate}
\item La poda de la solución optima va a seguir funcionando bien, y su efecto va a ser mayor sobre el árbol dado que vamos a descartar muchos mas nodos.
\item La poda de ver si la posición actual ya es alcanzable ya no va a servir para todas las piezas, dado que hay que verificar muchas mas posiciones. Si puede ser útil para los peones, los caballos y el rey.
\item Seria interesante intentar probar si algunas piezas tienen un posicionamiento óptimo. Por ejemplo, poniendo las torres en los costados podemos reducir la dimensión del problema. Lo mismo sucede con la reina.
\end{enumerate}

\subsubsection{Complejidad}
\begin{enumerate}
\item La complejidad de verificar si un tablero es solución aumentara de forma dramática. Esto se debe a que ahora un casillero puede ser accedido desde muchas mas posiciones. El análisis de los casos mas simples que antes era \order{1} ahora sera mucho mayor, no solo porque deberemos codificar el movimiento de todas las piezas del ajedrez, si no que también porque el cubrimiento dependerá de las posiciones de otras piezas en el tablero. Por ejemplo, una reina podría llegar a cierta posición de forma diagonal, pero solo si esa diagonal no esta ocupada por otra pieza. Tendremos que diseñar un nuevo algoritmo específicamente para verificar tableros.
\item Se le podría asignar a cada pieza una jerarquía, con el objetivo de recorrer un árbol de la forma mas eficiente posible para encontrar una solución, dado que ayuda a la poda del optimo. Primero ubicaríamos las reinas, luego las torres, luego los alfiles, los caballos, el rey y los peones.
\end{enumerate}
