#include <deque>
#include <iostream>
#include <math.h>
#include <string>
#include <chrono>
#include <ctime>

using namespace std;

typedef deque<int> Vec;
typedef deque<Vec> Board;

// function prototypes
string solve(Board& b, int currentHorseCount, int& optimalHorseCount);
void backtracking(Board& b, string& localSolution, int n, int n2, int currentHorseCount, int& optimalHorseCount, int currentSquare);
bool verify(const Board& b, int n);
bool reachable(const Board& b, int i, int j, int n);
string getUsedHorses(const Board& b);
bool trimTree(const Board& b, int i, int j, int n);

int main()
{
	int boardSize;
	int staticHorseCount;

	int horseRow;
	int horseColumn;

	cin >> boardSize >> staticHorseCount;

	Board b(boardSize, Vec(boardSize, 0)); // build board.

	// fill board
	for(int i = 0; i < staticHorseCount; i++) {
		cin >> horseRow >> horseColumn;
		b[horseRow - 1][horseColumn - 1] = 2;
	}

	int optimalHorseCount = 0;
	
	/*if(boardSize > 3) {
		optimalHorseCount = (boardSize * boardSize) / 3 + staticHorseCount; // worst case scenario.
	} else {
		optimalHorseCount = boardSize * boardSize;
	}*/
	optimalHorseCount = boardSize * boardSize;

	std::chrono::high_resolution_clock::time_point start, end;
    start = chrono::high_resolution_clock::now();
	string res = solve(b, staticHorseCount, optimalHorseCount);
	end = chrono::high_resolution_clock::now();
	int elapsed_microseconds = chrono::duration_cast<chrono::microseconds> (end-start).count();

	cout << optimalHorseCount - staticHorseCount << endl;

	cout << res;

	cout << "elapsed time: " << elapsed_microseconds << " microsecconds\n";

	return 0;

}

string solve(Board& b, int currentHorseCount, int& optimalHorseCount)
{

	int n  = b.size();
	int n2 = pow(n, 2); 

	if (verify(b, n) == true) { // already solved.
		optimalHorseCount = currentHorseCount;
		return getUsedHorses(b);
	}

	// set initial local solution.
	string localSolution;

	backtracking(b, localSolution, n, n2, currentHorseCount, optimalHorseCount, 0);

	return localSolution;

}

void backtracking(Board& b, string& localSolution, int n, int n2, int currentHorseCount, int& optimalHorseCount, int currentSquare)
{
	// we cant get a better solution.
	if (currentHorseCount + 1 == optimalHorseCount) return;

	// not enough horses.
	if (currentHorseCount + (n2 - currentSquare - 2) < n2/4) return;

	// no squares left to try.
	if (currentSquare == n2) return;

	int i = currentSquare / n; //row
	int j = currentSquare % n; //column

	if (b[i][j] == 2) { // static horse, skip it.
		backtracking(b, localSolution, n, n2, currentHorseCount, optimalHorseCount, currentSquare + 1);
		return;
	}
	
	backtracking(b, localSolution, n, n2, currentHorseCount, optimalHorseCount, currentSquare + 1);

	//if (trimTree(b, i, j, n)) return;

	b[i][j] = 1;

	if (verify(b, n) == true) { // solution found, no need to keep adding horses.
		if (optimalHorseCount > currentHorseCount) {
			localSolution = getUsedHorses(b);
			optimalHorseCount = currentHorseCount + 1;
		}
	} else {
		backtracking(b, localSolution, n, n2, currentHorseCount + 1, optimalHorseCount, currentSquare + 1);
	}

	b[i][j] = 0;

}

bool verify(const Board& b, int n)
{
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			if (!reachable(b, i, j, n)) return false;
		}
	}
	return true;
}

bool trimTree(const Board& b, int i, int j, int n)
{

	// check if upper squares have a reachable horse.
	if (!(i - 2 >= 0 && j - 1 >= 0 && b[i-2][j-1] > 0)) return false;
	if (!(i - 2 >= 0 && j + 1 <  n && b[i-2][j+1] > 0)) return false;	
	if (!(i - 1 >= 0 && j - 2 >= 0 && b[i-1][j-2] > 0)) return false;
	if (!(i - 1 >= 0 && j + 2 <  n && b[i-1][j+2] > 0)) return false;

	// check if lower squares have a reachable horse.
	if (!(i + 1 < n  && j - 2 >= 0 && b[i+1][j-2] > 0)) return false;
	if (!(i + 2 < n  && j - 1 >= 0 && b[i+2][j-1] > 0)) return false;
	if (!(i + 1 < n  && j + 2 < n  && b[i+1][j+2] > 0)) return false;
	if (!(i + 2 < n  && j + 1 < n  && b[i+2][j+1] > 0)) return false;
	
	// if we reached here, it means the square is unreachable.
	return true;
}

bool reachable(const Board& b, int i, int j, int n)
{

	// check if square already has a horse in it.
	if (b[i][j]) return true;

	// check if upper squares have a reachable horse.
	if (i - 2 >= 0 && j - 1 >= 0 && b[i-2][j-1] > 0) return true;
	if (i - 2 >= 0 && j + 1 <  n && b[i-2][j+1] > 0) return true;	
	if (i - 1 >= 0 && j - 2 >= 0 && b[i-1][j-2] > 0) return true;
	if (i - 1 >= 0 && j + 2 <  n && b[i-1][j+2] > 0) return true;

	// check if lower squares have a reachable horse.
	if (i + 1 < n  && j - 2 >= 0 && b[i+1][j-2] > 0) return true;
	if (i + 2 < n  && j - 1 >= 0 && b[i+2][j-1] > 0) return true;
	if (i + 1 < n  && j + 2 < n  && b[i+1][j+2] > 0) return true;
	if (i + 2 < n  && j + 1 < n  && b[i+2][j+1] > 0) return true;
	
	// if we reached here, it means the square is unreachable.
	return false;
}

string getUsedHorses(const Board& b)
{
	string output = "";
	int n = b.size();
	for(int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			if (b[i][j] == 1) output += to_string(i+1) + " " + to_string(j+1) + "\n";
		}
	}
	return output;
}
