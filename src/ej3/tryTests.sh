#!/bin/bash

for i in $( ls tests ); do
	if [[ "$i" == *".in" ]]; then
		echo ---------------------
		echo Testing $i
		echo "$(./caballos < tests/$i)"
 	else
 		echo "$(cat tests/$i)"
	fi
done