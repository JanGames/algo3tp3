#include <iostream>
#include <math.h>

using namespace std;

int main() {

	int exercice = 0;

	int seed = 0;
	int size = 0;
	int param = 0;
	string test = "";

	cin >> exercice >> seed >> size >> param;

	//First exercice
	if(exercice == 1) {
		//Get random seed from time
		srand(seed);

		int money = rand() % size * 1000 + size * 100;
		//Cout cities and money
		test += to_string(size) + " ";
		test += to_string(money) + " ";
		test += "\n"; //End Line

		//Create random values (Range 1-100)
		for(int i = 0; i < size; i++) {
			test += to_string(rand() % 100) + " "; //Zombies
			test += to_string(rand() % 100) + " "; //Soldiers
			test += to_string(rand() % 100 + 1) + " "; //Soldiers cost
			test += "\n"; //End Line
		}
	}
	//All cities winned
	if(exercice == 10) {
		int money = 1;
		//Cout cities and money
		test += to_string(size) + " ";
		test += to_string(money) + " ";
		test += "\n"; //End Line

		//Create basic values
		for(int i = 0; i < size; i++) {
			test += to_string(1) + " "; //Zombies
			test += to_string(1) + " "; //Soldiers
			test += to_string(1) + " "; //Soldiers cost
			test += "\n"; //End Line
		}
	}
	//All cities lost
	if(exercice == 100) {
		int money = size;
		//Cout cities and money
		test += to_string(size) + " ";
		test += to_string(money) + " ";
		test += "\n"; //End Line

		//Create basic values
		for(int i = 0; i < size; i++) {
			test += to_string(1) + " "; //Zombies
			test += to_string(0) + " "; //Soldiers
			test += to_string(1) + " "; //Soldiers cost
			test += "\n"; //End Line
		}
	}
	//Second exercice
	if(exercice == 2) {
		//Get random seed from time
		srand(seed);

		//Cout ammount of frecuencies
		test += to_string(size) + " ";
		test += "\n"; //End Line

		//Get starting time
		int ftime = rand() % 5;
		//Create random values (Range 1-100)
		for(int i = 0; i < size; i++) {
			test += to_string(rand() % 100 + 1) + " "; //Costo
			test += to_string(ftime) + " "; //Inicio
			ftime += rand() % 4 + 1; //Offset time
			test += to_string(ftime) + " "; //Fin
			ftime += rand() % 4 + 1; //Offset time
			test += "\n"; //End Line
		}
	}
	if(exercice == 20) {
		//Cout ammount of frecuencies
		test += to_string(size) + " ";
		test += "\n"; //End Line

		//Create basic values
		for(int i = 0; i < size; i++) {
			test += to_string(1) + " "; //Costo
			test += to_string(i * 2) + " "; //Inicio
			test += to_string(i * 2 + 1) + " "; //Fin
			test += "\n"; //End Line
		}
	}
	//Third exercice
	if(exercice == 3) {
		//Get random seed from time
		srand(seed);

		//Create the horses string and count them
		string horses = "";
		int count = 0;
		for(int i = 1; i <= size; i++) {
			for(int j = 1; j <= size; j++) {
				if(rand() % 10 < 2) {
					horses += to_string(i) + " " + to_string(j) + "\n"; //Costo
					count++;
				}
			}
		}

		//Cout size of board
		test += to_string(size) + " " + to_string(count) + "\n";
		test += horses; //Add horses string
	}
	if(exercice == 30) {
		//Cout size of board and 
		test += to_string(size) + " " + to_string(0) + "\n";
	}
	if(exercice == 300) {
		//Get random seed from time
		srand(seed);

		int board = size * size;

		//Create the horses string and count them
		int count = 0;
		/*int array [board];
		for(int i = 1; i < size; i++) {
			for(int j = 1; j < size; j++) {
				array[i * size + j] = 0;
			}
		}*/

		string horses = "";
		for(int i = 0; i <= param && count <= board; i++) {
			horses += to_string((int)(i - floor(i / size) * size) + 1) + " " + to_string(int(floor(i / size) + 1)) + "\n";
			count++;
		}

		//Cout size of board
		test += to_string(size) + " " + to_string(count) + "\n";
		test += horses; //Add horses string
	}

	cout << test;

	return 0;

}