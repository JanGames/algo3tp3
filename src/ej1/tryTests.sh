#!/bin/bash

for i in $( ls tests ); do
	if [[ "$i" == *".in" ]]; then
		echo ---------------------
		echo Testing $i
		echo "$(./zombies < tests/$i)"
 	else
 		echo "$(cat tests/$i)"
	fi
done
