#include <algorithm>
#include <deque>
#include <iostream>
#include <math.h>
#include <tuple>

using namespace std;

typedef tuple<int, int, int> Ciudad;
typedef deque<Ciudad> Pais;

int main() {
        
	int cantidadDeCiudades;
	int presupuestoDisponible;

	int zombiesEnCiudad;
	int soldadosEnCiudad;
	int costoDeEnvioUnitario;
	int costoDeRecuperacion;

	int refuerzosNecesarios;

	cin >> cantidadDeCiudades >> presupuestoDisponible;
        
        Pais pais (cantidadDeCiudades);

		/* Ingresar todas las ciudades al vector pais. Solo ingresaremos los datos relevantes,
		* que son el id de la ciudad y el costo de limpieza. */
		for(int i = 1; i <= cantidadDeCiudades; i++) {

		cin >> zombiesEnCiudad >> soldadosEnCiudad >> costoDeEnvioUnitario;

		refuerzosNecesarios = ceil(zombiesEnCiudad / 10.0) - soldadosEnCiudad;
		
		if (refuerzosNecesarios < 0) refuerzosNecesarios = 0;

		costoDeRecuperacion = refuerzosNecesarios * costoDeEnvioUnitario;

		Ciudad nuevaCiudad = make_tuple(costoDeRecuperacion, i, refuerzosNecesarios);
                
                pais[i - 1] = nuevaCiudad;

	}

	make_heap(pais.begin(), pais.end());
	sort_heap(pais.begin(), pais.end());

	unsigned int i = 0;
	int ciudadesLimpias = 0;
	int soldadosEnviados[cantidadDeCiudades];

	while(i < pais.size()) {

		if (get<0>(pais[i]) <= presupuestoDisponible) {
			//limpiar ciudad
			presupuestoDisponible -= get<0>(pais[i]);
			soldadosEnviados[get<1>(pais[i]) - 1] = get<2>(pais[i]);
			if (get<0>(pais[i]) >= 0) ciudadesLimpias++;
		} else {
			soldadosEnviados[get<1>(pais[i]) - 1] = 0;
		}

		i++;

	}
	cout  << ciudadesLimpias << " ";

	for(int i = 0; i < cantidadDeCiudades; i++) cout << soldadosEnviados[i] << " ";
	cout << endl;

	return 0;
}
