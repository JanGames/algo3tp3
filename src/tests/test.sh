#!/bin/bash
# GO TO THE BOTOM TO SET TESTS

#Clear output file

#Create basic input files for generator
echo "1 1 1 0" > exercice1 #Zombies
echo "2 1 1 0" > exercice2 #Frecuencies
echo "3 1 1 0" > exercice3 #Horses, board with pieces
echo "10 1 1 0" > exercice10 #Zombies basic all cities cleared
echo "20 1 1 0" > exercice20 #Frecuencies basic
echo "30 1 1 0" > exercice30 #Horses, board without pieces
echo "100 1 1 0" > exercice100 #Zombies basic all cities lost
echo "20 1 1 0" > exercice200 #Frecuencies basic same as 20
echo "300 1 1 1" > exercice300 #Horses, board without pieces, last parameters is ammount of pieces

#Clear the txt of some test
#Params
	#$1 ejecutable
	#$2 exercice number
function CLEAR {
	#Clear the output files
	echo "N,T (ns),TDN" > "output${1}${2}.csv"
	> "output${1}${2}.log"
}

#Get time in nanoseconds
#Params
	#None
function TIME {
	date +%s%N
}

#Test the predefined test cases
#Params
	#$1 tests list
	#$2 ejecutable
function PREDEFINED {
	echo "Predefined tests: ${#tests[*]}"
	echo "Begin predefined tests"

	for i in ${tests[*]}
	do
		echo "Predefine test: ${i}"

		echo "${i}" >> "output${2}predef.log"

		otime=$(TIME) #Time before calling ejecutable
		./$2 < $i >> "output${2}predef.log"
		ctime=$(TIME) #Time after calling ejecutable
		dtime=$((ctime - otime)) #Calculate delta time
		echo "${i},${dtime}" >> "output${2}predef.csv"

		echo "Test time: ${dtime}ns"
	done
}

#Run the program with the desired params
#Params
	#$1 ejecutable
	#$2 ejercice number
	#$3 size of test
function RUN {

	wait
	otime=$(TIME) #Time before calling ejecutable
	./$1 < generator.txt >> "output${1}${2}.log"
	ctime=$(TIME) #Time after calling ejecutable
	dtime=$((ctime - otime)) #Calculate delta time
	printf ",${dtime}" >> "output${1}${2}.csv"

	if [ "${1}" == "zombies" ] || [ "${1}" == "frecuencias" ]
	then
		printf "," >> "output${1}${2}.csv"
		float=`echo "${dtime} / ( ${3} * ( l(${3}) / l(2) ) )" | bc -l`
		echo ${float%.*}  >> "output${1}${2}.csv"
	else
		echo ",0" >> "output${1}${2}.csv"
	fi

	echo "Time: ${dtime}ns"
}

#Make a input with $2 size and test output $1 ammount of times
#Params
	#$1 ammount of tests
	#$2 size of tests
	#$3 program ejecutable
	#$4 exercice number
	#$5 special param
function TEST {
	echo "Starting $1 random test/s with ${3} (Exercice ${4}), size ${2}"

	for i in `seq 1 ${1}`
	do
		echo $i

		t="$(TIME)"
		echo "${4} $((i + t % 1000)) ${2} ${5}" > exercice$4
		./generator < exercice$4 > generator.txt 
		./generator < exercice$4 >> "output${3}${4}.log"
		printf "${2}" >> "output${3}${4}.csv"
		RUN $3 $4 $2

		echo " " >> "output${3}${4}.log"
	done
}

#Run a lots of test (made with the generator) of and ejecutable
	#$1 ammount of tests per size
	#$2 program ejecutable
	#$3 exercice number
function REPEATTESTS {
	for i in ${sizes[*]}
	do
		TEST $1 $i $2 $3 0
	done
}

function REPEATHORSES {
	echo "Special test for ejercice 3"
	echo "Running ${1} basic ${2} test/s with sizes: ${sizes}"

	for i in `seq 10 ${1}`
	do
		echo $i

		for j in `seq 1 10`
		do
			t="$(TIME)"
			echo "${3} $((i + t % 1000)) 7 ${i}" > exercice$4
			./generator < exercice$4 > generator.txt 
			./generator < exercice$4 >> "output${2}${3}.log"
			printf "${i}" >> "output${2}${3}.csv"

			wait
			otime=$(TIME) #Time before calling ejecutable
			./$2 < generator.txt >> "output${2}${3}.log"
			ctime=$(TIME) #Time after calling ejecutable
			dtime=$((ctime - otime)) #Calculate delta time
			printf ",${dtime}" >> "output${2}${3}.csv"

			echo ",0" >> "output${2}${3}.csv"

			echo "Time: ${dtime}ns"

			echo " " >> "output${2}${3}.log"
		done
	done
}

#Run tests
#Zombies E1
# CLEAR zombies1
# CLEAR zombies10
# CLEAR zombies100
# sizes=(500 600 700 800 900 1000 1100 1200 1300 1400 1500 1600 1700 1800 1900 2000 2250 2500 2750 3000 3250 3500 3750 4000 4250 4500 4750 5000 5500 6000 6500 7000 7500 8000 8500 9000 9500 10000 11000 12000 13000 14000 15000 16000 17000 18000 19000 20000 22500 25000 27500 30000 32500 35000 37500 40000 42500 45000 47500 50000)
# REPEATTESTS 10 zombies 1   #Random test
# REPEATTESTS 10 zombies 10  #Always wins
# REPEATTESTS 10 zombies 100 #Always lost

#Frecuencias E2
#CLEAR frecuencias
#sizes=(100 150 200 250 300 400 500 600 700 800 900 1000 1100 1200 1300 1400 1500 1600 1700 1800 1900 2000 2250 2500 2750 3000 3250 3500 3750 4000 4250 4500 4750 5000)
#REPEATTESTS 10 frecuencias 2
#REPEATTESTS 10 frecuencias 20

#Horses E3
#CLEAR caballos3
#CLEAR caballos30
#CLEAR caballos300
sizes=(1 2 3 4 5 6)
#sizes=(7)
#REPEATTESTS 10 caballos 3
#REPEATTESTS 10 caballosOpt 30
#REPEATTESTS 10 caballosOptMin 30
#REPEATTESTS 10 caballosOptMinTrim 30
REPEATTESTS 10 caballosOptInfSupMin 30
#REPEATTESTS 10 caballosNoPodas 30
#REPEATHORSES 48 caballos 300

#tests=(zombies.txt zombies1.txt zombies2.txt zombies3.txt)
tests=

#PREDEFINED tests