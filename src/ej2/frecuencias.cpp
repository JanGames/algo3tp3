#include <cstdio>
#include <deque>
#include <iostream>
#include <math.h>
#include <tuple>

using namespace std;

typedef tuple<int,int,int,int> Frecuencia;
typedef deque<Frecuencia> ListaDeFrecuencias;

ListaDeFrecuencias Divide(ListaDeFrecuencias);
ListaDeFrecuencias Conquer(ListaDeFrecuencias, ListaDeFrecuencias);

int main(){

    int cantidadDeFrecuencias;

    int costo;
    int inicio;
    int fin;


	cin >> cantidadDeFrecuencias;

    ListaDeFrecuencias frecuenciasIn;

	for(int i = 1; i <= cantidadDeFrecuencias; i++) {

		cin >> costo >> inicio >> fin;

		if (inicio >= 0 && fin >= 0 && costo >= 0 && (fin > inicio)){

            Frecuencia frecuencia = make_tuple(inicio, costo, fin, i);
            frecuenciasIn.push_back(frecuencia);
		}
	}

    frecuenciasIn = Divide(frecuenciasIn);

    int CostoTotal = 0;

    for(int i = 0; i < (int)frecuenciasIn.size(); i++){

        CostoTotal += get<1>(frecuenciasIn[i])*(get<2>(frecuenciasIn[i]) - get<0>(frecuenciasIn[i]));
    }

    cout << CostoTotal;

    for(int i = 0; i < (int)frecuenciasIn.size(); i++){

            cout << endl << get<0>(frecuenciasIn[i]) << " " << get<2>(frecuenciasIn[i]) << " " << get<3>(frecuenciasIn[i]);
    }

    cout << endl << -1;

    return 0;
}

ListaDeFrecuencias Divide(ListaDeFrecuencias frecuenciasIn){

    if (frecuenciasIn.size() <= 1)
        return frecuenciasIn;

    ListaDeFrecuencias left(frecuenciasIn.begin(), frecuenciasIn.begin() + ceil(frecuenciasIn.size()/2));
    ListaDeFrecuencias right(frecuenciasIn.begin() + ceil(frecuenciasIn.size()/2), frecuenciasIn.end());

    left = Divide(left);
    right = Divide(right);

    return Conquer(left, right);
}

ListaDeFrecuencias Conquer(ListaDeFrecuencias left, ListaDeFrecuencias right){

    ListaDeFrecuencias listOut;
    Frecuencia aux;
    Frecuencia interseccionFrec;
    int inicio = 0;
    int costo = 0;
    int fin = 0;
    int numero = 0;
    bool terminoFrec = true;
    bool registrar = false;
    int iT = (get<0>(left[0]) <= get<0>(right[0])) ? get<0>(left[0]) : get<0>(right[0]);
    int iLeft = 0;
    int iRight = 0;
    int iOut = 0;
    int maxT = (get<2>(left[left.size() - 1]) < get<2>(right[right.size() - 1])) ? get<2>(right[right.size() - 1]) : get<2>(left[left.size() - 1]);
    bool select = true;
    bool interseccion = false;


    while( iT < maxT ){

        //Elige entre los topes de las "pilas":
        if ( terminoFrec ){

            terminoFrec = false;

            if ( iRight >= (int)right.size()){

                fin = get<2>(left[iLeft]);
                select = true;
                inicio = (get<0>(left[iLeft]) > iT) ? get<0>(left[iLeft]) : iT;
                costo = get<1>(left[iLeft]);
                numero = get<3>(left[iLeft]);

            } else if ( iLeft >= (int)left.size()){

                fin = get<2>(right[iRight]);
                select = false;
                inicio = (get<0>(right[iRight]) > iT) ? get<0>(right[iRight]) : iT;
                costo = get<1>(right[iRight]);
                numero = get<3>(right[iRight]);

                } else {

                    if ( get<0>(right[iRight]) < get<0>(left[iLeft])){

                        fin = get<2>(right[iRight]);
                        select = false;
                        inicio = (get<0>(right[iRight]) > iT) ? get<0>(right[iRight]) : iT;
                        costo = get<1>(right[iRight]);
                        numero = get<3>(right[iRight]);

                        }else if (get<0>(left[iLeft]) < get<0>(right[iRight])){

                            fin = get<2>(left[iLeft]);
                            select = true;
                            inicio = (get<0>(left[iLeft]) > iT) ? get<0>(left[iLeft]) : iT;
                            costo = get<1>(left[iLeft]);
                            numero = get<3>(left[iLeft]);

                            }else if ( get<1>(right[iRight]) < get<1>(left[iLeft])){

                                fin = get<2>(right[iRight]);
                                select = false;
                                inicio = (get<0>(right[iRight]) > iT) ? get<0>(right[iRight]) : iT;
                                costo = get<1>(right[iRight]);
                                numero = get<3>(right[iRight]);

                                }else{

                                    fin = get<2>(left[iLeft]);
                                    select = true;
                                    inicio = (get<0>(left[iLeft]) > iT) ? get<0>(left[iLeft]) : iT;
                                    costo = get<1>(left[iLeft]);
                                    numero = get<3>(left[iLeft]);
                                }
                }
        }

        if ( select ){

            if( iRight < (int)right.size() ){

                // Hay interseccion ?
                if ( (inicio >= get<0>(left[iLeft])) && (get<0>(right[iRight]) < get<2>(left[iLeft]) ) ){

                    // Es mas barata ?
                    if ( get<1>(right[iRight]) < get<1>(left[iLeft]) ){

                        fin = get<0>(right[iRight]);
                        registrar = true;
                        interseccion = true;
                        select = false;

                        get<0>(interseccionFrec) = get<0>(right[iRight]);
                        get<1>(interseccionFrec) = get<1>(right[iRight]);
                        get<2>(interseccionFrec) = get<2>(right[iRight]);
                        get<3>(interseccionFrec) = get<3>(right[iRight]);

                        if ( get<2>(right[iRight]) >= get<2>(left[iLeft]) ) iLeft++;
                    }
                    else if ( get<2>(right[iRight]) <= get<2>(left[iLeft]) ) iRight++;
                        else  if ( get<2>(right[iRight]) >= get<2>(left[iLeft]) ){

                        fin = get<2>(left[iLeft]);
                        registrar = true;
                        terminoFrec = true;
                        iLeft++;
                    }

                } else {

                fin = get<2>(left[iLeft]);
                registrar = true;
                terminoFrec = true;
                iLeft++;

                }
            } else {

                fin = get<2>(left[iLeft]);
                registrar = true;
                terminoFrec = true;
                iLeft++;
            }

        } else {

            if( iLeft < (int)left.size() ){

                // Hay interseccion ?
                if  ( (get<0>(left[iLeft]) >= inicio) && (get<0>(left[iLeft]) < get<2>(right[iRight]) ) ){

                    // Es mas barata ?
                    if ( get<1>(left[iLeft]) < get<1>(right[iRight]) ){

                        fin = get<0>(left[iLeft]);
                        registrar = true;
                        interseccion = true;
                        select = true;

                        get<0>(interseccionFrec) = inicio;
                        get<1>(interseccionFrec) = get<1>(left[iLeft]);
                        get<2>(interseccionFrec) = get<2>(left[iLeft]);
                        get<3>(interseccionFrec) = get<3>(left[iLeft]);

                        if ( get<2>(left[iLeft]) >= get<2>(right[iRight]) ) iRight++;
                    }
                    else if ( get<2>(left[iLeft]) <= get<2>(right[iRight]) ) iLeft++;
                        else if ( get<2>(left[iLeft]) >= get<2>(right[iRight]) ){

                        fin = get<2>(right[iRight]);
                        registrar = true;
                        terminoFrec = true;
                        iRight++;
                    }

                } else {

                        fin = get<2>(right[iRight]);
                        registrar = true;
                        terminoFrec = true;
                        iRight++;
                }

            } else {

                fin = get<2>(right[iRight]);
                registrar = true;
                terminoFrec = true;
                iRight++;
            }

        }

        //Registra el tramo de frecuencia en la salida:
        if ( registrar ){

            registrar = false;
            get<0>(aux) = inicio;
            get<1>(aux) = costo;
            get<2>(aux) = fin;
            get<3>(aux) = numero;

            iT = fin;
            
            if ( inicio < fin ) {
				
				listOut.push_back(aux);
				iOut++;
			}	

            if ( interseccion ){

                inicio = fin;
                interseccion = false;
                costo = get<1>(interseccionFrec);
                fin = get<2>(interseccionFrec);
                numero = get<3>(interseccionFrec);
            }
        }
    }

    return listOut;
}
